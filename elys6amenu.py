from tkinter import *
from tkinter import messagebox
import sqlite3
import elyscrud
#Creación de la pantalla principal
root=Tk()
root.title("Contabilidad de Licdo. Elys Santaella")
root.geometry("350x300")
root.iconbitmap(bitmap="favico.ico")
root.configure(background='black')
#or more generally <widget>.configure(background='black').
root.resizable(0, 0)
#Declaración d elas variables
myAccountID=StringVar()
myLogin=StringVar()
########################################################
###################FUNCIONES EN GENERAL
########################################################
def mostrarLicencia():
        print("Esto es Software 100% Libre")
def mostrarAutor():
        print("Copyleft(2021): José Santaella jsantaella70")
################BASES DE DATOS
#Creación de la base de datos y su tabla
def crearDataBase():
    miConexion=sqlite3.connect("contableDB")
    miCursor=miConexion.cursor()
    try:
        miCursor.execute('''
            CREATE TABLE accountTB(
            accountID VARCHAR(10) PRIMARY KEY,
            login VARCHAR(50))
            ''')
        messagebox.showinfo("DB creada con éxito")
    except:
        messagebox.showwarning("¡Hey!","DB already exist")

def salirAplicacion():
    valor=messagebox.askquestion("Salir","¿Deseas salir de la aplicación?")
    if valor=="yes":
        root.destroy()

## Resetear los valores de los campos
def limpiarCampos():
    myAccountID.set("")
    myLogin.set("")
## CRUD: Create(crear un registro e incluirlo)
def crear():
    miConexion=sqlite3.connect("contableDB")
    miCursor=miConexion.cursor()
    datos=myAccountID.get(),myLogin.get()
    miCursor.execute("SELECT * FROM accountTB WHERE accountID=" +myAccountID.get())
    exist = miCursor.fetchone()
    print(exist)
    if exist is None:
        print('Nuevo usuario')
        miCursor.execute("INSERT INTO accountTB VALUES(?,?)", (datos))
        miConexion.commit()
        messagebox.showinfo("Registro insertado con éxito")
    else:
        print('Usuario ya registrado')
#CRUD: Read(leer un registro y mostrar los campos consultados)
def leer():
    if (myAccountID.get()!=""):
        miConexion=sqlite3.connect("contableDB")
        miCursor=miConexion.cursor()
        miCursor.execute("SELECT * FROM accountTB WHERE accountID = " +myAccountID.get())
        elUsuario=miCursor.fetchall()
        for usuario in elUsuario:
            myAccountID.set(usuario[0])
            myLogin.set(usuario[1])
        miConexion.commit()
    else:
        print("campo vacíoooo")
#CRUD: Update(actualizar un registro al modificar según los valores de los widgets entry)
def actualizar():
    miConexion=sqlite3.connect("contableDB")
    miCursor=miConexion.cursor()
    datos=myAccountID.get(), myLogin.get()
    miCursor.execute("UPDATE accountTB SET accountID=?, login=? WHERE accountID="+myAccountID.get(),(datos))
    miConexion.commit()
    messagebox.showinfo("Registro actualizado con éxito")
#CRUD: Delete(borrar un registro según el ID del entry del Password)
def borrar():
    miConexion=sqlite3.connect("contableDB")
    miCursor=miConexion.cursor()
    miCursor.execute("DELETE FROM accountTB WHERE accountID = "+myAccountID.get())
    miConexion.commit()
    messagebox.showinfo("Registro borrado con éxito")
def listar():
    miConexion=sqlite3.connect("contableDB")
    miCursor=miConexion.cursor()
    miCursor.execute("SELECT * FROM accountTB")
    elUsuario=miCursor.fetchall()
    #imprimir aquie el numero de filas.. print('Número de filas', elUsuario.length())
    registerCounter=1
    for usuario in elUsuario:
        print(registerCounter,usuario[0]," ",usuario[1])
        registerCounter=registerCounter+1
    miConexion.commit()

## Ventana hija
def hija():
    ## Crea la ventana hija.
    t1 = Toplevel(root,bg="blue")

    ## Establece el tamaño para la ventana.
    t1.geometry('400x200+20+20')

    ## Provoca que la ventana tome el focus
    t1.focus_set()

    ## Deshabilita todas las otras ventanas hasta que
    ## esta ventana sea destruida.
    t1.grab_set()

    ## Indica que la ventana es de tipo transient, lo que significa
    ## que la ventana aparece al frente del padre.
    t1.transient(master=root)

    ## Crea un widget Label en la ventana
    l0=Label(t1, text='Ventana hija',bg="blue").pack()


    ## Crea los campos de entrada.
    l1=Label(t1, text="Passwd: ").pack()
    e1=Entry(t1, textvariable=myAccountID).pack()
    l2=Label(t1, text="Login: ").pack()
    e2=Entry(t1, textvariable=myLogin).pack()
    
    ## Crea los botones de la ventana
    b1=Button(t1, text="Create", command=crear).pack()
    b2=Button(t1, text="Read", command=leer).pack()
    b3=Button(t1, text="Update", command=actualizar).pack()
    b4=Button(t1, text="Delete", command=borrar).pack()
    b5=Button(t1, text="Limpiar", command=limpiarCampos).pack()
    b6=Button(t1, text="Listar", command=listar).pack()
    
    ## Crea un widget que permite cerrar la ventana,
    ## para ello indica que el comando a ejecutar es el
    ## metodo destroy de la misma ventana.
    b7=Button(t1,text="Cerrar",bg="green", command=t1.destroy).pack()
    ## Establece el focus en el entry.
    e1.focus()

    ## Pausa el mainloop de la ventana de donde se hizo la invocación.
    t1.wait_window(t1)
################FIN BASES DE DATOS
#####################################################
#Creación del menú desplegable
menubar=Menu(root)
#Submenú Cuentas
menuCuenta=Menu(menubar, tearoff=0)
menuCuenta.add_command(label="Clase", command=mostrarLicencia)
menuCuenta.add_command(label="Grupo", command=mostrarLicencia)
menuCuenta.add_command(label="Cuenta", command=mostrarLicencia)
menuCuenta.add_command(label="Subcuenta", command=mostrarLicencia)
menubar.add_cascade(label="Cuentas", menu=menuCuenta)
#Submenú Movimiento
menuMovimiento=Menu(menubar, tearoff=0)
menuMovimiento.add_command(label="Ventas", command=mostrarLicencia)
menuMovimiento.add_command(label="Compras", command=mostrarLicencia)
menubar.add_cascade(label="Movimientos", menu=menuMovimiento)
#Submenú Utilidades
menuUtilidad=Menu(menubar, tearoff=0)
menuUtilidad.add_command(label="Restaurar", command=mostrarLicencia)
menuUtilidad.add_command(label="Respaldar", command=mostrarLicencia)
menuUtilidad.add_command(label="Crear DB", command=crearDataBase)
menuUtilidad.add_command(label="Seguridad", command=hija)
menubar.add_cascade(label="Utilidades", menu=menuUtilidad)
#Submenú Ayuda
menuAyuda=Menu(menubar, tearoff=0)
menuAyuda.add_command(label="Licencia", command=mostrarLicencia)
menuAyuda.add_command(label="Acerca", command=mostrarAutor)
menubar.add_cascade(label="Ayuda", menu=menuAyuda)
root.config(menu=menubar)
#Contenido
#
#metodo del ciclo de la ventana principal
root.mainloop()
